<?php

return [
    'the_file' => 'el archivo',
    'name' => 'Nombre',
    'mime_type' => 'Tipo de archivo',
    'size' => 'Tamaño',
    'preview' => 'Previsualización'
];
