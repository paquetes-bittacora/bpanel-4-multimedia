<?php

return [
    'add' => 'Añadir archivo',
    'name' => 'Nombre',
    'attach_file' => 'Adjuntar fichero',
    'keywords' => 'Palabras clave',
    'title' => 'Título',
    'description' => 'Descripción',
    'alt' => 'Título alternativo',
    'list' => 'Listado de multimedia',
    'show' => 'Mostrando datos de archivo multimedia: ',
    'features' => 'Características',
    'file_name' => 'Nombre del archivo',
    'mime_type' => 'Tipo MIME',
    'extension' => 'Extensión',
    'edit' => 'Editando datos de archivo multimedia: ',
    'multiupload' => 'Subida múltiple de ficheros'
];
