<?php

return [
    'index' => 'Listar multimedia',
    'create' => 'Añadir multimedia',
    'show' => 'Mostrar multimedia',
    'edit' => 'Editar multimedia',
    'destroy' => 'Eliminar multimedia',
    'multiupload' => 'Subida múltiple',
    'bulkdelete' => 'Borrado en lotes'
];
