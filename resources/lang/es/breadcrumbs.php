<?php

return [
    'multimedia' => 'Multimedia',
    'create' => 'Añadir',
    'edit' => 'Editar',
    'index' => 'Listar',
    'multiupload' => 'Subida en lotes'
];
