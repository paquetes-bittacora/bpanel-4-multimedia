<?php

return [
    'file_mimes' => 'El formato de archivo adjuntado no está permitido',
    'title_required' => 'El título es obligatorio',
    'title_array' => 'Debe rellenar los títulos de todos los idiomas',
    'title_min_3' => 'El campo "título" debe tener, al menos, 3 caracteres de longitud',
    'file_max' => 'El archivo supera el tamaño máximo permitido de subida (Máx: ' . config('multimedia.max_file_size') . ' MB)',
    'file_required' => 'Debe adjuntar un archivo'

];
