@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Subida de ficheros múltiple')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('multimedia::form.multiupload') }}</span>
            </h4>
        </div>
        <div class="card-body">
                @livewire('form::input-file', ['name' => 'file[]', 'idField' => 'file', 'fieldWidth' => 12, 'required' => true, 'accept' => $allowedFormats, 'multiple' => true,
                'allowedFileExtensions' => $allowedExtensions, 'uploadUrl' => route('multimedia.multiuploadstore'), 'csrf' => true, 'showUpload' => false, 'showRemove' => true,
                'uploadAsync' => true, 'initialPreviewAsData' => true, 'overwriteInitial' => false])
        </div>
    </div>
@endsection
