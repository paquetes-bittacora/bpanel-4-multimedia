<div id="prueba">
    <div class="modal fade" id="multimediaImagesLibrary" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index:5000">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Biblioteca de imágenes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @livewire('multimedia::multimedia-images-library-table')
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            window.addEventListener("openMultimediaImagesLibraryModal", function(){
                $("#multimediaImagesLibrary").modal("show");
            });
            window.addEventListener("closeMultimediaImagesLibraryModal", function(){
                $("#multimediaImagesLibrary").modal("hide");
            });
        </script>
    @endpush
</div>
