<x-livewire-tables::bs4.table.cell>
        @livewire('utils::media-preview', ['media' => $row->getMedia('images')->first()], key('image-media-library-'.$row->id))
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    {{$row->getMedia('images')->first()->file_name}}
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    <button class="addImage btn btn-primary" wire:click="$emit('addImageFromMediaLibraryTinyMce', {{$row->id}})">Añadir</button>
</x-livewire-tables::bs4.table.cell>
