<div>
    <div class="text-right">
        <a class="btn btn-primary" wire:click="mediaLibraryTableModal">
            <i class="fa fa-plus-circle"></i> Biblioteca multimedia
        </a>
    </div>

    <div class="modal fade" id="mediaLibraryTableModal{{$type}}" tabindex="-1" aria-labelledby="mediaLibraryTableModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Biblioteca multimedia</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @livewire('multimedia::media-library-images-table', ['contentId' => $contentId, 'type' => $type])
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script>
        window.addEventListener("mediaLibraryTableModalOpen{{$type}}", function(){
            $("#mediaLibraryTableModal{{$type}}").modal("show");
        });
    </script>
@endpush
