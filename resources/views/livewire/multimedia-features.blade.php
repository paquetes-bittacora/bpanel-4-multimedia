<div class="container multimedia-features mb-4">
    <div class="row">
        <div class="col-sm-6">
            @if(in_array($fileExtension, $imageExtensions))
                <a href="{{$media->getUrl()}}" class="d-block text-center" data-fancybox="multimedia{{$media->id}}">
                    <img src="{{$media->getUrl('thumb-350')}}" alt="" class="w-50">
                </a>
            @else
                <div class="d-block text-center">
                    <img src="{{ asset("bpanel/icons/$fileExtension-512.png") }}" alt="" class="w-50">
                </div>
            @endif

        </div>
        <div class="col-sm-6">
            <table class="multimedia-table-features w-100">
                <thead>
                    <tr>
                        <th colspan="2">
                            {{ __('multimedia::form.features') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ __('multimedia::form.name') }}:</td>
                        <td>{{$media->name}}</td>
                    </tr>
                    <tr>
                        <td>{{ __('multimedia::form.file_name') }}: </td>
                        <td>{{$media->file_name}}</td>
                    </tr>
                    <tr>
                        <td>{{ __('multimedia::form.mime_type') }}:</td>
                        <td>{{$media->mime_type}}</td>
                    </tr>
                    <tr>
                        <td>{{ __('multimedia::form.extension') }}: </td>
                        <td>{{\Bittacora\Utils\UtilsFacade::extractExtensionFromFilename($media->file_name)}}</td>
                    </tr>
                    <tr>
                        <td>{{ __('multimedia::datatable.size') }}:</td>
                        <td>{{\Bittacora\Utils\UtilsFacade::convertFromBytes($media->size)}}</td>
                    </tr>
                    <tr>
                        <td>{{ __('multimedia::form.keywords') }}:</td>
                        <td>{{$multimedia->keywords}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
