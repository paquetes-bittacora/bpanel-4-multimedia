<td>
    <div class="text-center">
        @livewire('utils::media-preview', ['media' => $row->mediaModel], key('multimedia-media-preview-'.$row->id))
    </div>
</td>
<td>
    <div class="text-center">
        {{$row->mediaModel->name}}
    </div>
</td>
<td>
    <div class="text-center">
        @livewire('utils::format-icon', ['mimeType' => \Bittacora\Utils\UtilsFacade::extractExtensionFromFilename($row->mediaModel->file_name)], key('multimedia-mime-'.$row->id))
    </div>
</td>
<td>
    <div class="text-center">
        {{\Bittacora\Utils\UtilsFacade::convertFromBytes($row->mediaModel->size)}}
    </div>
</td>
<td>
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'multimedia', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el elemento?'], key('multimedia-buttons-'.$row->id))
    </div>
</td>
