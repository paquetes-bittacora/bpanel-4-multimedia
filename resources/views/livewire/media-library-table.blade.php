<td>
{{--    {{$row->attached}}--}}
    @if($type == "images")
        @livewire('utils::media-preview', ['media' => $row->getMedia('images')->first()], key('image-media-library-'.$row->id))
    @elseif($type == "documents")
        <div class="file-format-icon text-center">
            <img src="{{asset("assets_bpanel/icons/".\Bittacora\Utils\UtilsFacade::extractExtensionFromFilename($row->media->first()->file_name) .".png")}}" alt="">
        </div>
    @endif
</td>
@if($type == "documents")
    <td>
        {{$row->media->first()->file_name}}
    </td>
@endif
<td>
    @livewire('utils::model-translatable-attribute', ['model' => $row, 'attribute' => 'title'], key('image-media-library-attribute'.$row->id))
</td>
<td>
    @livewire('content-multimedia::toggle-content-multimedia-button', ['contentId' => $contentId, 'multimediaId' => $row->id, 'type' => $type], key('refreshToggleButton'.$row->id))
</td>
