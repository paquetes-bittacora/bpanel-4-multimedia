@extends('bpanel.layouts.bpanel-app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets_bpanel/css/multimedia.css') }}">
@endsection

@section('title', 'Editar archivo')

@section('content')
    <div class="card bcard multimedia-show">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('multimedia::form.edit') . ' ' . $multimedia->mediaModel->name}}</span>
            </h4>
        </div>
        <div class="card-body">
            <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('multimedia.update', $multimedia)}}" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                @livewire('multimedia::multimedia-features', ['multimedia' => $multimedia, 'media' => $multimedia->mediaModel, 'imageExtensions' => $imageExtensions,
                'fileExtension' => $fileExtension, 'arrayImages' => $multimedia->mediaModel])
                @livewire('form.input-text', ['name' => 'name', 'labelText' => __('multimedia::form.name'), 'value' => $multimedia->mediaModel->first()->name])
                @livewire('form.input-text', ['name' => 'keywords', 'labelText' => __('multimedia::form.keywords'), 'value' => $multimedia->keywords])
                <br>

                @foreach($languages as $language)
                    <div class="card-header bgc-secondary-d1 text-white border-0">
                        <h4 class="text-120 mb-0">
                            <span class="text-90">{{$language->name}}</span>
                        </h4>
                    </div>
                    <div class="card-body">
                        @if($errors->any())
                            @if(array_key_exists('title.'.$language->locale, $errors->messages()))
                                @livewire('form.input-text', ['name' => 'title['.$language->locale.']', 'labelText' => __('multimedia::form.title'),'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'required' => true, 'customErrorMessage' => $errors->messages()["title.$language->locale"][0],
                                'value' => $multimedia->getTranslation('title', $language->locale)])
                            @else
                                @livewire('form.input-text', ['name' => 'title['.$language->locale.']', 'labelText' => __('multimedia::form.title'),'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'required' => true, 'value' => $multimedia->getTranslation('title', $language->locale)])
                            @endif
                        @else
                            @livewire('form.input-text', ['name' => 'title['.$language->locale.']', 'labelText' => __('multimedia::form.title'),'alignIcon' => 'append',
                            'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'required' => true, 'value' => $multimedia->getTranslation('title', $language->locale)])
                        @endif

                        @if($errors->any())
                            @if(array_key_exists('description.'.$language->locale, $errors->messages()))
                                @livewire('form.textarea', ['name' => 'description['.$language->locale.']', 'labelText' => __('multimedia::form.description'), 'language' => $language->locale, 'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'customErrorMessage' => $errors->messages()["description.$language->locale"][0],
                                'value' => $multimedia->getTranslation('description', $language->locale)])
                            @else
                                @livewire('form.textarea', ['name' => 'description['.$language->locale.']', 'labelText' => __('multimedia::form.description'), 'language' => $language->locale, 'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'value' => $multimedia->getTranslation('description', $language->locale)])
                            @endif
                        @else
                            @livewire('form.textarea', ['name' => 'description['.$language->locale.']', 'labelText' => __('multimedia::form.description'), 'language' => $language->locale, 'alignIcon' => 'append',
                            'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'value' => $multimedia->getTranslation('description', $language->locale)])
                        @endif



                        @if($errors->any())
                            @if(array_key_exists('alt.'.$language->locale, $errors->messages()))
                                @livewire('form.input-text', ['name' => 'alt['.$language->locale.']', 'labelText' => __('multimedia::form.alt'),'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'customErrorMessage' => $errors->messages()["alt.$language->locale"][0], 'value' => $multimedia->getTranslation('alt', $language->locale)])
                            @else
                                @livewire('form.input-text', ['name' => 'alt['.$language->locale.']', 'labelText' => __('multimedia::form.alt'),'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'value' => $multimedia->getTranslation('alt', $language->locale)])
                            @endif
                        @else
                            @livewire('form.input-text', ['name' => 'alt['.$language->locale.']', 'labelText' => __('multimedia::form.alt'),'alignIcon' => 'append',
                            'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'value' => $multimedia->getTranslation('alt', $language->locale)])
                        @endif
                    </div>

                @endforeach

                @livewire('utils::created-updated-info', ['model' => $multimedia])

                <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                    @livewire('form.save-button',['theme'=>'update'])
                    @livewire('form.save-button',['theme'=>'reset'])
                </div>
            </form>

        </div>
    </div>
@endsection
