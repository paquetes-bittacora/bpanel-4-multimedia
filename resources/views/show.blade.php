@extends('bpanel.layouts.bpanel-app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets_bpanel/css/multimedia.css') }}">
@endsection

@section('title', 'Mostrar datos de archivo')

@section('content')
    <div class="card bcard multimedia-show">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('multimedia::form.show') . ' ' . $multimedia->mediaModel()->first()->name}}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('multimedia::multimedia-features', ['multimedia' => $multimedia, 'media' => $multimedia->mediaModel()->first(),
            'fileExtension' => $fileExtension, 'imageExtensions' => $imageExtensions])

            @foreach($languages as $key => $language)
                <div class="container multimedia-features">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="multimedia-table-features w-100">
                                <thead>
                                    <tr>
                                        <th colspan="2">
                                            <img src="{{ asset("assets_bpanel/flags/$language->locale.png") }}" alt=""> <span>{{ $language->name }}</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            {{ __('multimedia::form.title') }}
                                        </td>
                                        <td>
                                            {{ $multimedia->getTranslation('title', $language->locale) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ __('multimedia::form.description') }}
                                        </td>
                                        <td>
                                            {{ $multimedia->getTranslation('description', $language->locale) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ __('multimedia::form.alt') }}
                                        </td>
                                        <td>
                                            {{ $multimedia->getTranslation('alt', $language->locale) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach

            @livewire('utils::created-updated-info', ['model' => $multimedia])
        </div>
    </div>

@endsection

@section('scripts')
    <script>
    </script>
@endsection
