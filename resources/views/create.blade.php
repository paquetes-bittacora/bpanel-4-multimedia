@extends('bpanel.layouts.bpanel-app')

@section('title', 'Añadir archivo')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('multimedia::form.add') }}</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('multimedia.store')}}" enctype="multipart/form-data">
            @csrf

            @livewire('form.input-file', ['name' => 'file', 'labelText' => __('multimedia::form.attach_file'), 'required' => true, 'accept' => $allowedFormats,
            'maxFileCount' => 1, 'allowedFileExtensions' => $allowedExtensions])
            @livewire('form.input-text', ['name' => 'name', 'labelText' => __('multimedia::form.name')])
            @livewire('form.input-text', ['name' => 'keywords', 'labelText' => __('multimedia::form.keywords')])

            @foreach($languages as $language)
                    <div class="card-header bgc-secondary-d1 text-white border-0">
                        <h4 class="text-120 mb-0">
                            <span class="text-90">{{$language->name}}</span>
                        </h4>
                    </div>
                    <div class="card-body">
{{--                        @if($errors->any())--}}
{{--                            {{dd($errors)}}--}}
{{--                        @endif--}}
                        @if($errors->any())
                            @if(array_key_exists('title.'.$language->locale, $errors->messages()))
                                @livewire('form.input-text', ['name' => 'title['.$language->locale.']', 'labelText' => __('multimedia::form.title'),'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'required' => true, 'customErrorMessage' => $errors->messages()["title.$language->locale"][0]])
                            @else
                                @livewire('form.input-text', ['name' => 'title['.$language->locale.']', 'labelText' => __('multimedia::form.title'),'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'required' => true])
                            @endif
                        @else
                            @livewire('form.input-text', ['name' => 'title['.$language->locale.']', 'labelText' => __('multimedia::form.title'),'alignIcon' => 'append',
                            'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'required' => true])
                        @endif

                        @if($errors->any())
                            @if(array_key_exists('description.'.$language->locale, $errors->messages()))
                                @livewire('form.textarea', ['name' => 'description['.$language->locale.']', 'labelText' => __('multimedia::form.description'), 'language' => $language->locale, 'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'customErrorMessage' => $errors->messages()["description.$language->locale"][0]])
                            @else
                                @livewire('form.textarea', ['name' => 'description['.$language->locale.']', 'labelText' => __('multimedia::form.description'), 'language' => $language->locale, 'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png')])
                            @endif
                        @else
                            @livewire('form.textarea', ['name' => 'description['.$language->locale.']', 'labelText' => __('multimedia::form.description'), 'language' => $language->locale, 'alignIcon' => 'append',
                            'image' => asset('assets_bpanel/flags/'.$language->locale.'.png')])
                        @endif



                        @if($errors->any())
                            @if(array_key_exists('alt.'.$language->locale, $errors->messages()))
                                @livewire('form.input-text', ['name' => 'alt['.$language->locale.']', 'labelText' => __('multimedia::form.alt'),'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png'), 'customErrorMessage' => $errors->messages()["alt.$language->locale"][0]])
                            @else
                                @livewire('form.input-text', ['name' => 'alt['.$language->locale.']', 'labelText' => __('multimedia::form.alt'),'alignIcon' => 'append',
                                'image' => asset('assets_bpanel/flags/'.$language->locale.'.png')])
                            @endif
                        @else
                            @livewire('form.input-text', ['name' => 'alt['.$language->locale.']', 'labelText' => __('multimedia::form.alt'),'alignIcon' => 'append',
                            'image' => asset('assets_bpanel/flags/'.$language->locale.'.png')])
                        @endif
                    </div>

                @endforeach

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form.save-button',['theme'=>'save'])
                @livewire('form.save-button',['theme'=>'reset'])
            </div>
        </form>
    </div>

@endsection
