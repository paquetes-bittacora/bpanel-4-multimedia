<?php

namespace Bittacora\Multimedia;

use Illuminate\Support\Facades\Config;

class Multimedia
{
    public function getAll(){
        return Models\Multimedia::with('mediaModel')->orderBy('created_at', 'DESC')->get();
    }

    public function getImageExtensions(){
        return Models\Multimedia::$imageExtensions;
    }

    public function getDocumentExtensions(){
        return Models\Multimedia::$documentExtensions;
    }

    public function getCompressedExtensions(){
        return Models\Multimedia::$compressedExtensions;
    }

    public function registerMediaConversion(callable $callable) {
        $currentConversions = config('media-conversions') ?? [];
        $currentConversions[] = $callable;
        Config::set('media-conversions', $currentConversions);
    }
}
