<?php /** @noinspection PhpSuperClassIncompatibleWithInterfaceInspection */

namespace Bittacora\Multimedia\Models;

use Bittacora\Content\Models\ContentModel;
use Bittacora\Content\Models\ContentMultimedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Translatable\HasTranslations;
use Wildside\Userstamps\Userstamps;

class Multimedia extends Model implements HasMedia
{
    use HasFactory, HasTranslations, interactsWithMedia, Userstamps;

    public $fillable = ['title', 'description', 'alt', 'keywords'];
    public $translatable = ['title', 'description', 'alt'];

    protected $with = ['mediaModel'];


    public function content(){
        return $this->belongsTo(ContentModel::class, 'id_model');
    }

    public static $allowedFormats = 'image/svg+xml, image/svg, image/jpeg, image/png, image/gif, application/pdf, application/msword, application/vnd.oasis.opendocument.text, application/vnd.oasis.opendocument.spreadsheet, application/vnd.ms-excel, application/zip, application/x-zip-compressed, application/x-rar-compressed, application/x-7z-compressed';
    public static $allowedExtensions = 'svg,jpg,jpeg,png,gif,pdf,doc,docx,odt,ods,xls,xlsx,rar,zip,7z';
    public static $imageExtensions = ['svg', 'jpg', 'jpeg', 'png', 'gif'];
    public static $imageFormats = 'image/svg+xml, image/jpeg, image/png, image/gif';
    public static $documentExtensions = ['pdf', 'doc', 'xls', 'xlsx', 'odt', 'ods', 'docx'];
    public static $documentFormats = 'application/pdf, application/msword, application/vnd.oasis.opendocument.text, application/vnd.oasis.opendocument.spreadsheet, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    public static $compressedExtensions = ['rar', 'zip', '7z'];
    public static $compressedFormats = 'application/x-zip-compressed, application/x-rar-compressed, application/x-7z-compressed';

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('images')->acceptsMimeTypes([
            'image/jpeg', 'image/png', 'image/gif', 'image/svg+xml', 'image/svg'
        ])->useDisk('images');

        $this->addMediaCollection('documents')->acceptsMimeTypes([
            'application/pdf', 'application/msword', 'application/vnd.oasis.opendocument.text',
            'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        ])->useDisk('documents');

        $this->addMediaCollection('compressed')->acceptsMimeTypes([
            'application/zip', 'application/x-rar-compressed', 'application/x-7z-compressed'
        ])->useDisk('compressed');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        foreach (config('media-conversions') as $mediaConversion) {
            $mediaConversion($this);
        }
    }

    public function mediaModel(){
        return $this->hasOne(MediaModel::class, 'model_id');
    }

}
