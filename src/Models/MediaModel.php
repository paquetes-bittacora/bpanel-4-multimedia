<?php

namespace Bittacora\Multimedia\Models;

use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaModel extends Media
{
    public function multimedia(){
        return $this->belongsTo(Multimedia::class, 'id');
    }
}
