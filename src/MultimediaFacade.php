<?php

namespace Bittacora\Multimedia;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Multimedia\Multimedia
 */
class MultimediaFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Multimedia::class;
    }
}
