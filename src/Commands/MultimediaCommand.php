<?php

namespace Bittacora\Multimedia\Commands;

use Illuminate\Console\Command;

class MultimediaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bpanel4-multimedia:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Registra los discos necesarios para el módulo de multimedia';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = config_path('filesystems.php');
        $originalContent = file_get_contents($path);
        if (str_contains($originalContent, '~@bp4MultimediaStart@~')) {
            return 0;
        }
        $search = "'disks' => [";
        $replace = $search . "
        // ~@bp4MultimediaStart@~
        'images' => [
            'driver' => 'local',
            'root' => storage_path('app/public/media/images'),
            'url' => env('APP_URL') . '/storage/media/images'
        ],
        'documents' => [
            'driver' => 'local',
            'root' => storage_path('app/public/media/documents'),
            'url' => env('APP_URL') . '/storage/media/documents'
        ],
        'compressed' => [
            'driver' => 'local',
            'root' => storage_path('app/public/media/compressed'),
            'url' => env('APP_URL') . '/storage/media/compressed'
        ],
        // ~@bp4MultimediaEnd@~";
        $newContent = str_replace($search, $replace, $originalContent);
        file_put_contents(config_path('filesystems.php'), $newContent);
    }
}
