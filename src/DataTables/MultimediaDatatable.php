<?php

namespace Bittacora\Multimedia\DataTables;

use Bittacora\Multimedia\Models\MediaModel;
use Bittacora\Multimedia\Models\Multimedia;
use Bittacora\Utils\Http\Livewire\BulkDeleteCheckbox;
use Bittacora\Utils\HttpLivewire\BulkDelete;
use Bittacora\Utils\UtilsFacade;
use Illuminate\Support\Facades\Config;
use Livewire\ComponentConcerns\ReceivesEvents;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class MultimediaDatatable extends DataTable
{
    public $checkedRecords = [];

    protected $actions = [];

    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('bulkDelete', function($multimedia){
//                return view('utils::livewire.bulk-delete-checkbox')->with(['value' => $multimedia->id]);
                return \Livewire::mount('utils::bulk-delete-checkbox', ['value' => $multimedia->id, 'modelName' => get_class($multimedia)])->html();
            })
            ->addColumn('preview', function($multimedia){
                return view('utils::livewire.media-preview')->with([
                    'media' => $multimedia->mediaModel()->first()
                ]);
            })
            ->addColumn('name', function($multimedia){
                return $multimedia->mediaModel()->first()->name;
            })
            ->addColumn('mime_type', function($multimedia){
                return view('utils::livewire.format-icon')->with([
                    'mimeType' => UtilsFacade::extractExtensionFromFilename($multimedia->mediaModel()->first()->file_name)
                ]);
            })
            ->addColumn('size', function($multimedia){
                return UtilsFacade::convertFromBytes($multimedia->mediaModel()->first()->size);
            })
            ->addColumn('action', function (Multimedia $multimedia) {
                return view('utils::partials.datatable-button')->with([
                    'buttons' => [
                        'show', 'edit', 'destroy'
                    ],
                    'model' => $multimedia,
                    'scope' => 'multimedia'
                ]);
            })->rawColumns(['bulkDelete']);
    }

    public function query(Multimedia $multimedia)
    {
        return $multimedia->newQuery();
    }

    public function html()
    {
        return $this->builder()
            ->rowId('id') //Necesario para el reorder
            ->addTableClass('d-style w-100 table text-dark-m1 text-95 border-y-1 brc-black-tp11 collapsed')
            ->columns($this->getColumns())
            ->parameters(['buttons' => $this->actions])
            ->paging(true)
            ->minifiedAjax()
            ->searching(true)
            ->dom("<'row'<'col-4'l><'col-5'f><'col-3'B>><'row'<'col-12't>><'row'<'col-12'p>><'clear'>")
            ->language('/vendor/datatables/spanish.json')
            ->deferLoading()
            ->drawCallbackWithLivewire();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('bulkDelete')->title(\Livewire::mount('utils::bulk-delete-all')->html())
                ->width("50px")
                ->orderable(false)
                ->searchable(false)
                ->className('text-center'),
            Column::make('preview')->title(__('multimedia::datatable.preview'))
                ->width("100px")
                ->orderable(false)
                ->searchable(false),
            Column::make('name')->title(__('multimedia::datatable.name'))->searchable(false)->orderable(false),
            Column::make('mime_type')->title(__('multimedia::datatable.mime_type'))
                ->addClass('text-center')
                ->orderable(false)
                ->searchable(false),
            Column::make('size')->title(__('multimedia::datatable.size'))
                ->addClass('text-center')
                ->orderable(false)
                ->searchable(false),
            Column::computed('action')->title('Acciones')
                ->printable(false)
                ->exportable(false)
                ->searchable(false)
                ->width('100px')
                ->addClass('bgc-white bgc-h-blue-l5 shadow-sm text-center')
        ];
    }
}
