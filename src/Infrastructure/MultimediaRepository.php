<?php

namespace Bittacora\Multimedia\Infrastructure;

use Bittacora\Multimedia\Http\Requests\StoreMultimediaRequest;
use Bittacora\Multimedia\Http\Requests\UpdateMultimediaRequest;
use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\ErrorHandler\Error\FatalError;

class MultimediaRepository
{
    public function create(StoreMultimediaRequest $request){
        try{
            $model = new Multimedia();

            $validatedFields = $request->validated();
            $file = $validatedFields['file'];
            $keywords = $validatedFields['keywords'];
            $name = $validatedFields['name'];


            unset($validatedFields['file']);
            unset($validatedFields['keywords']);
            unset($validatedFields['name']);

            foreach($validatedFields as $arrayKey => $arrayValues){
                foreach($arrayValues as $languageKey => $languageValue){
                    if(!is_null($languageValue)){
                        $model->setTranslation($arrayKey, $languageKey, $languageValue);
                    }
                }
            }

            $model->setAttribute('keywords', $keywords);

            $model->save();

            if(in_array($file->getClientOriginalExtension(), Multimedia::$imageExtensions)){
                $media = $model->addMedia($file)->toMediaCollection('images');
            }elseif(in_array($file->getClientOriginalExtension(), Multimedia::$documentExtensions)){
                $media = $model->addMedia($file)->toMediaCollection('documents');
            }else{
                $media = $model->addMedia($file)->toMediaCollection('compressed');
            }

            if(!is_null($name)){
                $media->setAttribute('name', $name);
                $media->save();
            }

            if($model) {
                Session::put('alert-success', 'El contenido multimedia ha sido añadido correctamente.');
            }
        } catch(\Exception $e) {
            report($e);
            Session::put('alert-danger', 'Hubo un error al añadir el contenido multimedia');
        }
    }

    public function update(UpdateMultimediaRequest $request, Multimedia $multimedia){
        try{
            $validatedFields = $request->validated();

            $keywords = $validatedFields['keywords'];
            $name = $validatedFields['name'];


            unset($validatedFields['keywords']);
            unset($validatedFields['name']);

            foreach($validatedFields as $arrayKey => $arrayValues){
                foreach($arrayValues as $languageKey => $languageValue){
                    if(!is_null($languageValue)){
                        $multimedia->setTranslation($arrayKey, $languageKey, $languageValue);
                    }
                }
            }

            $multimedia->setAttribute('keywords', $keywords);

            $multimedia->update();

            $media = $multimedia->mediaModel()->first();

            if(!is_null($name)){
                $media->setAttribute('name', $name);
                $media->update();
            }

            if($multimedia and $media){
                Session::put('alert-success', 'El contenido multimedia ha sido editado correctamente.');
            }
        } catch(\Exception $e) {
            report($e);
            Session::put('alert-danger', 'Hubo un error al editar el contenido multimedia');
        }
    }

    public function bulkUpload(array $file){
        try{
            $file = new UploadedFile(
                $file['tmp_name'][0],
                $file['name'][0]
            );

            $multimedia = Multimedia::create();

            if(in_array($file->getClientOriginalExtension(), Multimedia::$imageExtensions)){
                $media = $multimedia->addMedia($file)->toMediaCollection('images');
            }elseif(in_array($file->getClientOriginalExtension(), Multimedia::$documentExtensions)){
                $media = $multimedia->addMedia($file)->toMediaCollection('documents');
            }else{
                $media = $multimedia->addMedia($file)->toMediaCollection('compressed');
            }

            return json_encode('ok');
        } catch(FatalError $e) {
            report($e);
            $multimedia->deleteMedia($media->id);
            $multimedia->delete();
            return json_encode('Ha superado el límite de memoria establecido en la configuración del servidor.');
        }catch(\Exception $e){
            report $e;
            $multimedia->deleteMedia($media->id);
            $multimedia->delete();
            return json_encode('Error en la subida del fichero');
        }
    }
}
