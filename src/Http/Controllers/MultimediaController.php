<?php

namespace Bittacora\Multimedia\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Multimedia\DataTables\MultimediaDatatable;
use Bittacora\Multimedia\Http\Requests\StoreMultimediaRequest;
use Bittacora\Multimedia\Http\Requests\UpdateMultimediaRequest;
use Bittacora\Multimedia\Infrastructure\MultimediaRepository;
use Bittacora\Multimedia\Models\Multimedia;
use Bittacora\Utils\UtilsFacade;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;

class MultimediaController extends Controller
{

    private $multimediaRepository;

    public function __construct(MultimediaRepository $multimediaRepository)
    {
        $this->multimediaRepository = $multimediaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('multimedia.index');
        return view('multimedia::index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->authorize('multimedia.create');
        $allowedFormats = Multimedia::$allowedFormats;
        $allowedExtensions = Multimedia::$allowedExtensions;
        return view('multimedia::create', compact('allowedFormats', 'allowedExtensions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreMultimediaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMultimediaRequest $request)
    {
        $this->authorize('multimedia.create');
        $this->multimediaRepository->create($request);
        return redirect(route('multimedia.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Multimedia $multimedia)
    {
        $this->authorize('multimedia.show');
        $imageExtensions = Multimedia::$imageExtensions;
        $fileExtension = UtilsFacade::extractExtensionFromFilename($multimedia->mediaModel()->first()->file_name);
        return view('multimedia::show')->with([
           'multimedia' => $multimedia,
            'imageExtensions' => $imageExtensions,
            'fileExtension' => $fileExtension
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Multimedia $multimedia)
    {
        $this->authorize('multimedia.edit');
        $imageExtensions = Multimedia::$imageExtensions;
        $fileExtension = UtilsFacade::extractExtensionFromFilename($multimedia->mediaModel->file_name);
        return view("multimedia::edit", compact('multimedia', 'imageExtensions', 'fileExtension'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateMultimediaRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMultimediaRequest $request, Multimedia $multimedia)
    {
        $this->authorize('multimedia.edit');
        $this->multimediaRepository->update($request, $multimedia);
        return redirect(route('multimedia.edit', compact('multimedia')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Multimedia $multimedia)
    {
        $this->authorize('multimedia.destroy');
        $multimedia->delete();
        Session::put('alert-success', 'El contenido multimedia ha sido eliminado correctamente.');
        return redirect(route('multimedia.index'));

    }

    public function multiUpload(){
        $this->authorize('multimedia.multiupload');
        $allowedFormats = Multimedia::$allowedFormats;
        $allowedExtensions = Multimedia::$allowedExtensions;
        return view("multimedia::multiupload", compact('allowedFormats', 'allowedExtensions'));
    }

    public function multiUploadStore(){
        $this->authorize('multimedia.multiupload');
        return $this->multimediaRepository->bulkUpload($_FILES['file']);
    }

    public function bulkDelete(Request $request){
        $this->authorize('multimedia.bulkdelete');
        $idsToDelete = $request->input('ids');
        if(empty($idsToDelete)){
            return json_encode('No se han seleccionado elementos.');
        }

        foreach($idsToDelete as $idToDelete){
            $multimedia = Multimedia::findOrFail($idToDelete);
            $multimedia->delete();
        }

        return json_encode("ok");
    }

    public function singleUploadStore(){
        $file = $_FILES['file'];
        $file = new UploadedFile(
            $file['tmp_name'],
            $file['name']
        );
        $multimedia = Multimedia::create();

        if(in_array($file->getClientOriginalExtension(), Multimedia::$imageExtensions)){
            $media = $multimedia->addMedia($file)->toMediaCollection('images');
        }elseif(in_array($file->getClientOriginalExtension(), Multimedia::$documentExtensions)){
            $media = $multimedia->addMedia($file)->toMediaCollection('documents');
        }else{
            $media = $multimedia->addMedia($file)->toMediaCollection('compressed');
        }

        if($media and $multimedia){
            echo json_encode(['location' => $media->getFullUrl()]);
            die();
        }
    }
}
