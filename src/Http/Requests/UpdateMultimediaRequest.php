<?php

namespace Bittacora\Multimedia\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMultimediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|array',
            'title.*' => 'required|string|min:3',
            'description' => 'array|nullable',
            'description.*' => 'nullable',
            'alt' => 'array|nullable',
            'alt.*' => 'nullable',
            'keywords' => 'nullable',
            'name' => 'nullable'
        ];

        return $rules;
    }

    public function messages(){
        $messages = [
            'title.required' => __('multimedia::validation.title_required'),
            'title.array' => __('multimedia::validation.title_array'),
            'title.min' => __('multimedia::validation.title_min_3'),
            'title.*.required' => __('multimedia::validation.title_required'),
            'title.*.min' => __('multimedia::validation.title_min_3'),
        ];

        return $messages;
    }
}
