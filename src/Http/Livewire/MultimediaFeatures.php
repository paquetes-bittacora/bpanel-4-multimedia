<?php

namespace Bittacora\Multimedia\Http\Livewire;

use Bittacora\Multimedia\Models\MediaModel;
use Bittacora\Multimedia\Models\Multimedia;
use Livewire\Component;

class MultimediaFeatures extends Component
{
    public MediaModel $media;
    public Multimedia $multimedia;
    public array $imageExtensions = [];
    public ?string $fileExtension = null;

    public function render()
    {
        return view('multimedia::livewire.multimedia-features')->with([
            'media' => $this->media,
            'multimedia' => $this->multimedia,
            'imageExtensions' => $this->imageExtensions,
            'fileExtension' => $this->fileExtension
        ]);
    }
}
