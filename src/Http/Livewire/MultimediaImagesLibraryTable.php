<?php

namespace Bittacora\Multimedia\Http\Livewire;

use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class MultimediaImagesLibraryTable extends DataTableComponent
{

    public bool $showSearch = false;
    public bool $showPerPage = false;
    public bool $reorderEnabled = false;
    public bool $reordering = false;
    public array $perPageAccepted = [5,10,25,50];
    public int $perPage = 5;

    public function getQueryString()
    {
        return [];
    }

    public function columns(): array
    {
        $columns = [];

        $columns[] = Column::make('Previsualización')->addClass('w-10');
        $columns[] = Column::make('Título')->addClass('w-80');

        $columns[] = Column::make('Acciones')->addClass('w-10 text-center');
        return $columns;
    }

    public function query(): Builder
    {
        $query = Multimedia::query()->orderBy('created_at', 'DESC')->with('media')->whereHas('media', function ($query) {
            $query->where('collection_name', 'images');
        });

        return $query;
    }

    public function rowView(): string
    {
        return "multimedia::livewire.multimedia-images-library-table";
    }
}
