<?php

namespace Bittacora\Multimedia\Http\Livewire;

use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class MultimediaDatatable extends DataTableComponent
{

    public bool $showSearch = false;
    public function columns(): array
    {
        return [
            Column::make(__('multimedia::datatable.preview'))->addClass('w-20 text-center'),
            Column::make(__('multimedia::datatable.name'))->addClass('w-30 text-center'),
            Column::make('MIME Type')->addClass('w-20 text-center'),
            Column::make('Tamaño')->addClass('w-20 text-center'),
            Column::make('Acciones')->addClass('w-10 text-center'),
        ];
    }

    public function query(): Builder
    {
        return Multimedia::query();
    }

    public function rowView(): string
    {
        return 'multimedia::livewire.multimedia-datatable';
    }

    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar'
        ];
    }

    public function bulkDelete(){
        if(count($this->selectedKeys())){
            Multimedia::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }
}
