<?php

namespace Bittacora\Multimedia\Http\Livewire;

use Bittacora\Multimedia\Models\Multimedia;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use function view;

class MultimediaImagesLibrary extends Component
{
    use LivewireAlert;
    protected $listeners = ['tinyMceOpenModal', 'addImageFromMediaLibraryTinyMce', 'refreshMultimediaImagesLibrary' => '$refresh'];

    public function tinyMceOpenModal(){
        $this->dispatchBrowserEvent('openMultimediaImagesLibraryModal');
    }

    public function addImageFromMediaLibraryTinyMce($id){
        $multimedia = Multimedia::where('id', $id) ->with('media') -> first();
        $this->dispatchBrowserEvent('addUrlFromMediaLibraryToTinyMce', ['url' => $multimedia->mediaModel->getUrl()]);
        $this->dispatchBrowserEvent('closeMultimediaImagesLibraryModal');
    }

    public function render()
    {
        return view('multimedia::livewire.multimedia-images-library');
    }


}
