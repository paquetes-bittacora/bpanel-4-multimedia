<?php

namespace Bittacora\Multimedia\Http\Livewire;

use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class MediaLibraryImagesTable extends DataTableComponent
{

    public int $contentId;
    public string $type;

    public bool $showSearch = false;
    public bool $showPerPage = false;
    public bool $reorderEnabled = false;
    public bool $reordering = false;
    public array $perPageAccepted = [5,10,25,50];
    public int $perPage = 5;

    public function mount($type){
        $this->type = $type;
    }
    public function getQueryString()
    {
        return [];
    }

    public function columns(): array
    {
        $columns = [];

        if($this->type == "images"){
            $columns[] = Column::make('Previsualización')->addClass('w-10');
            $columns[] = Column::make('Título')->addClass('w-80');
        }elseif($this->type == "documents"){
            $columns[] = Column::make('Formato')->addClass('w-10');
            $columns[] = Column::make('Fichero')->addClass('w-15');
            $columns[] = Column::make('Título')->addClass('w-65');
        }

        $columns[] = Column::make('Acciones')->addClass('w-10 text-center');
        return $columns;
    }

    public function query(): Builder
    {
        $query = Multimedia::query()->orderBy('created_at', 'DESC')->with('media')->whereHas('media', function ($query) {
            $query->where('collection_name', $this->type);
        });

        return $query;
    }

    public function rowView(): string
    {
        return "multimedia::livewire.media-library-table";
    }

}
