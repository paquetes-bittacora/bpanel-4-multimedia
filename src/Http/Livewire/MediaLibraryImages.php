<?php

namespace Bittacora\Multimedia\Http\Livewire;

use Livewire\Component;

class MediaLibraryImages extends Component
{
    public int $contentId;
    public string $type;


    protected $listeners = ['mediaLibraryTableModal'];

    protected function getListeners()
    {
        return ["mediaLibraryTableModal$this->type" => "mediaLibraryTableModal"];
    }

    public function mount($type){
        $this->type = $type;
    }

    public function render()
    {
        return view('multimedia::livewire.media-library-images');
    }

    public function mediaLibraryTableModal(){
        $this->dispatchBrowserEvent("mediaLibraryTableModalOpen$this->type");
    }
}
