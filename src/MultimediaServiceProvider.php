<?php

namespace Bittacora\Multimedia;

use Bittacora\Multimedia\Http\Livewire\MediaLibraryImages;
use Bittacora\Multimedia\Http\Livewire\MediaLibraryImagesTable;
use Bittacora\Multimedia\Http\Livewire\MultimediaDatatable;
use Bittacora\Multimedia\Http\Livewire\MultimediaFeatures;
use Bittacora\Multimedia\Http\Livewire\MultimediaImagesLibrary;
use Bittacora\Multimedia\Http\Livewire\MultimediaImagesLibraryTable;
use Illuminate\Support\Facades\Config;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Multimedia\Commands\MultimediaCommand;
use Bittacora\Multimedia\MultimediaFacade;

class MultimediaServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('multimedia')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_multimedia_table')
            ->hasCommand(MultimediaCommand::class);
    }

    public function register()
    {

        $this->registerMediaConversions();

        $this->app->bind('multimedia', function ($app) {
            return new Multimedia();
        });

        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'multimedia');
    }

    public function boot()
    {

        $this->commands(MultimediaCommand::class);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'multimedia');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'multimedia');

        Livewire::component('multimedia::multimedia-features', MultimediaFeatures::class);
        Livewire::component('multimedia::media-library-images', MediaLibraryImages::class);
        Livewire::component('multimedia::media-library-images-table', MediaLibraryImagesTable::class);
        Livewire::component('multimedia::multimedia-images-library', MultimediaImagesLibrary::class);
        Livewire::component('multimedia::multimedia-images-library-table', MultimediaImagesLibraryTable::class);
        Livewire::component('multimedia::multimedia-datatable', MultimediaDatatable::class);

        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('multimedia.php'),
        ], 'config');

    }

    private function registerMediaConversions(): void
    {
        MultimediaFacade::registerMediaConversion(function ($media) {
            $media->addMediaConversion('thumb-351')
                ->width(349)
                ->height(222)
                ->keepOriginalImageFormat()
                ->crop('crop-center', 349, 223)
                ->performOnCollections('images');
        });

        MultimediaFacade::registerMediaConversion(function ($media) {
            $media->addMediaConversion('project-directory')
                ->width(340)
                ->height(280)
                ->keepOriginalImageFormat()
                ->crop('crop-center', 340, 281)
                ->performOnCollections('images');
        });

        MultimediaFacade::registerMediaConversion(function ($media) {
            $media->addMediaConversion('service-home')
                ->width(302)
                ->height(183)
                ->keepOriginalImageFormat()
                ->crop('crop-center', 302, 184)
                ->performOnCollections('images');
        });

        MultimediaFacade::registerMediaConversion(function ($media) {
            $media->addMediaConversion('thumb-preview')
                ->width(74)
                ->height(74)
                ->keepOriginalImageFormat()
                ->performOnCollections('images');
        });
    }
}
