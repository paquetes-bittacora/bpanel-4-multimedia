<?php

use Bittacora\Multimedia\Http\Controllers\MultimediaController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel')->middleware(['web', 'auth', 'admin-menu'])->name('multimedia.')->group(function(){
    Route::get('/multimedia', [MultimediaController::class, 'index'])->name('index');
    Route::get('/multimedia/create', [MultimediaController::class, 'create'])->name('create');
    Route::get('/multimedia/{multimedia}/show', [MultimediaController::class, 'show'])->name('show');
    Route::get('/multimedia/{multimedia}/edit', [MultimediaController::class, 'edit'])->name('edit');
    Route::post('/multimedia/store', [MultimediaController::class, 'store'])->name('store');
    Route::put('/multimedia/{multimedia}', [MultimediaController::class, 'update'])->name('update');
    Route::delete('/multimedia/{multimedia}', [MultimediaController::class, 'destroy'])->name('destroy');
    Route::get('/multimedia/multiupload', [MultimediaController::class, 'multiUpload'])->name('multiupload');
    Route::post('/multimedia/multiuploadstore', [MultimediaController::class, 'multiUploadStore'])->name('multiuploadstore');
    Route::post('/multimedia/singleuploadstore', [MultimediaController::class, 'singleUploadStore'])->name('singleuploadstore');
    Route::post('/multimedia/bulkdelete', [MultimediaController::class, 'bulkDelete'])->name('bulkdelete');
    Route::post('/multimedia/deletefromupload', [MultimediaController::class, 'deleteFromUpload'])->name('deletefromupload');
});
