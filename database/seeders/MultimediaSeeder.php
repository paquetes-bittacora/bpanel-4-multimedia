<?php

namespace Bittacora\Multimedia\Database\Seeders;

use Bittacora\AdminMenu\AdminMenuFacade;
use Bittacora\Tabs\Tabs;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class MultimediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = AdminMenuFacade::createModule('configuration', 'multimedia', 'Multimedia', 'fas fa-photo-video');
        AdminMenuFacade::createAction($module->key, 'Listar', 'index', 'fa fa-bars');
        AdminMenuFacade::createAction($module->key, 'Añadir', 'create', 'fa fa-plus');
//        AdminMenuFacade::createAction($module->key, 'Subida múltiple', 'multiupload', 'fa fa-upload');

        $roleAdmin = Role::findByName('admin');

        $permissions = ['index', 'create', 'show', 'edit', 'destroy', 'multiupload', 'bulkdelete'];

        foreach($permissions as $permission){
            $permission = Permission::create(['name' => 'multimedia.'.$permission]);
            $roleAdmin->givePermissionTo($permission);
        }

        Tabs::createItem('multimedia.index', 'multimedia.index', 'multimedia.index', 'Multimedia','fa fa-list');
        Tabs::createItem('multimedia.index', 'multimedia.create', 'multimedia.create', 'Nuevo','fa fa-plus');
//        Tabs::createItem('multimedia.index', 'multimedia.multiupload', 'multimedia.multiupload', 'Subida múltiple','fas fa-upload');

        Tabs::createItem('multimedia.create', 'multimedia.index', 'multimedia.index', 'Multimedia','fa fa-list');
        Tabs::createItem('multimedia.create', 'multimedia.create', 'multimedia.create', 'Nuevo','fa fa-plus');
//        Tabs::createItem('multimedia.create', 'multimedia.multiupload', 'multimedia.multiupload', 'Subida múltiple','fas fa-upload');

        Tabs::createItem('multimedia.edit', 'multimedia.index', 'multimedia.index', 'Multimedia','fa fa-list');
        Tabs::createItem('multimedia.edit', 'multimedia.create', 'multimedia.create', 'Nuevo','fa fa-plus');
//        Tabs::createItem('multimedia.edit', 'multimedia.multiupload', 'multimedia.multiupload', 'Subida múltiple','fas fa-upload');
    }
}
