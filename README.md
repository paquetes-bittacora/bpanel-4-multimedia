# bPanel4 Multimedia

## Instalación

Este paquete es compatible con el autoinstalador de bPanel4, así que para instalarlo
solo hay que añadirlo como dependencia en el composer.json de otro paquete, o ejecutar

```
composer require bittacora/bpanel4-multimedia
```

## Registrar conversiones

Para registrar conversiones desde otro paquete, hay que llamar a `registerMediaConversion`
a través de la fachada del módulo, pasando una función anónima como la siguiente:

```php
MultimediaFacade::registerMediaConversion(function ($media) {
    $media->addMediaConversion('thumb-351')
        ->width(349)
        ->height(222)
        ->keepOriginalImageFormat()
        ->crop('crop-center', 349, 223)
        ->performOnCollections('images');
});
```

De este bloque de código sólo hay que cambiar las llamadas que se hacen al objeto `$media`. Puedes
ver qué opciones hay disponibles en la [documentación de Spatie Media Library](https://spatie.be/docs/laravel-medialibrary/v10/converting-images/defining-conversions). Un buen sitio para llamar a este método sería el service provider del paquete que necesita definir un tamaño.

**Nota**: lo que se hace internamente es añadir estas funciones anónimas a un array al que el
modelo `Media` llamará, pasando `$this` como parámetro a la función anónima. Por lo tanto,
`$media` solo es el nombre del parámetro de la función, no es necesario que exista ninguna
variable `$media` ni nada donde llamamos a `registerMediaConversion`.
